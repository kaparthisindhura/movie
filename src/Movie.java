public class Movie {
    final private String title;
    final private String studio;
    final private String rating;
    static int pgCount=0;
    Movie(String title,String studio,String rating)
    {
        this.title=title;
        this.studio=studio;
        this.rating=rating;
    }
    Movie(String title,String studio)
    {
        this.title=title;
        this.studio=studio;
        this.rating="PG";
    }
    static Movie[] getPg(Movie[] movieArr)
    {
        Movie[] pgMovieArr = new Movie[movieArr.length];
        for (Movie movie : movieArr) {
            if (movie.rating.equals("PG")) {
                pgMovieArr[pgCount] = movie;
                pgCount++;
            }
        }
        return pgMovieArr;
    }

    public static void main(String[] args) {
        Movie[] movieArr=new Movie[5];
        Movie[] pgMoveArr;
        Movie mov1=new Movie("Casino Royal","Eon productions","PG-13");
        Movie mov2=new Movie("Interstellar","A productions","PG");
        Movie mov3=new Movie("Inception","B productions");
        Movie mov4=new Movie("Big Bang Theory","C productions","PG");
        Movie mov5=new Movie("Casino Royal2","Eon productions","PG-13");
        movieArr[0]=mov1;
        movieArr[1]=mov2;
        movieArr[2]=mov3;
        movieArr[3]=mov4;
        movieArr[4]=mov5;

        pgMoveArr =Movie.getPg(movieArr);
        for(int i=0;i< pgCount;i++)
        {
            System.out.println(pgMoveArr[i].title+" "+ pgMoveArr[i].studio+" "+ pgMoveArr[i].rating);
        }

    }
}
